# BioPro EEG-task-server

An EEG monitoring service built on Raspberry Pi that analyzes EEG data in real-time and allows doctors to see if there are any abnormalities and confirm the data on a web UI at any time.

### Hardware

- Raspberry Pi4

### System Design

```mermaid
graph LR
A[main.py] -- create process --> B[ApiManager]
A -- create process --> C[MqttManager]
B -- connect --> D[(Database)]
C -- connect --> D
```

- main.py is the main manager of the system, responsible for the start-up and shutdown of the entire system.
- main.py initializes ApiManager and MqttManager and create corresponding processes.
- ApiManager and MqttManager will perform initialization settings, connect to the database, and start operating in their own setup function.

### Run Service

```
$ python main.py
# It will run MQTT client and API server on http://0.0.0.0:3030
```

#### API manager

```
class TaskContentResponse(BaseModel):
    id: int
    name: str
    desc: str
    date: str
    data: str
    checked: bool = False

@app.get('/task/all')
# return { 'data': [TaskContent, ...]}. Due to the latency issue, it only returns the latest 100 tasks.

@app.delete('/task/{id}')
# delete target task from task list

@app.patch('/task/update/{id}')
# update target task
```

#### MQTT manager

Connect to MQTT broker and receive EEG data from device.

```
def on_message(self, client: mqtt.Client, user_data, message: mqtt.MQTTMessage):
# Receive data from MQTT broker. It pass the data to DataAnalyzer for processing and add tasks to the database.
```

#### Database (PostgreSQL)

Database manager based on sqlalchemy.orm, connect to DB and send sql command. It provides database access for ApiManager and MqttManager.

### Target

We set up a server on Raspberry Pi to receive real-time neural signals from an EEG device via MQTT. The EEG device sends 200 samples of data every 200ms. The server analyzes the signals in real-time and adds a task to the "task" table in the database with the following format when the signal value exceeds a threshold (>= 2000):

```
id: task id,
name: Activation Detect,
data: [234, 236, 12, ... 2004, ...-23, 211, 134] - 501 data points including 250 samples before and after the signal value that exceeded the threshold,
desc: The signal is [2004, the value that exceeded the threshold], and the average value before and after the signal is [234, the average value of task.data],
date: The time when the task is created, in the format of "2023-04-28",
checked: Whether the task has been checked by the doctor in the UI.
```

Example:

```
id: 0,
name: Activation Detect,
data: [234, 236, 12, ... 2004, ...-23, 211, 134]
desc: The signal is 2004, and the average value before and after the signal is 234.,
date: 2023-04-28,
checked: False
```

On the other hand, when no signal value exceeds the threshold for 120 seconds, a task is added as follows:

```
id: task id,
name: Sleep Detect,
data: [234, 236, 12, ...-23, 211, 134] - the latest 501 data points,
desc: The average value of the signal is [103, the average value of task.data],
date: The time when the task is created, in the format of "2023-04-28",
checked: Whether the task has been checked by the doctor in the UI.
```

Note: If an Activation Detect event (signal value >= threshold) occurs, the subsequent 250 samples will no longer be compared with the threshold. In other words, there will only be one event that occurs in a window size (501 samples), which will be added as a task.

