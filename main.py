import logging
from multiprocessing import Process, current_process
from time import sleep

from api.api_manager import ApiManager
from mqtt.mqtt_manager import MqttManager

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] %(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def initialize_managers():
    api_manager = ApiManager()
    mqtt_manager = MqttManager()
    logging.info(f'{current_process().name} initialized')
    return api_manager, mqtt_manager


def start_manager(manager):
    try:
        manager.setup()
        logging.info(f'{manager.__class__.__name__} starts at {current_process().name}')
        while True:
            try:
                sleep(10)
            except:
                break
    except Exception as e:
        logging.error(f'{manager.__class__.__name__} encountered an exception: {e}')


def shutdown_manager(manager):
    try:
        manager.shutdown()
        logging.info(f'{manager.__class__.__name__} shutdown')
    except Exception as e:
        logging.error(f'{manager.__class__.__name__} encountered an exception: {e}')


def main():
    api_manager, mqtt_manager = initialize_managers()

    api_process = Process(target=start_manager, args=(api_manager,))
    mqtt_process = Process(target=start_manager, args=(mqtt_manager,))

    api_process.start()
    mqtt_process.start()

    api_process.join()
    mqtt_process.join()

    shutdown_manager(api_manager)
    shutdown_manager(mqtt_manager)


if __name__ == '__main__':
    main()
