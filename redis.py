import redis

class Redis():
    """Manage redis connection.
    """

    def __init__(self):
        self._host = '127.0.0.1'
        self._port = 6379
        self._password = 'BioProControlBox'
        self._connection = None
        self._key_prefix = 'biopro:'

    def open(self):
        """Setup redis and start thread.
        """
        self._connection = redis.Redis(
            host=self._host,
            port=self._port,
            password=self._password,
        )

    def close(self):
        """Thread shutdown and close engine.
        """
        self._connection.close()

    def keys(self, pattern: str):
        return self._connection.keys(f'{self._key_prefix}{pattern}')

    def delete(self, key: str):
        if self._key_prefix in key:
            self._connection.delete(key)
        else:
            self._connection.delete(f'{self._key_prefix}{key}')

    def get(self, key: str) -> bytes:
        if self._key_prefix in key:
            return self._connection.get(key)
        else:
            return self._connection.get(f'{self._key_prefix}{key}')

    def set(self, key: str, value: str, expire: int = None):
        if not isinstance(value, (str, int, bytes)):
            value = str(value)
        if self._key_prefix in key:
            self._connection.set(key, value, expire)
        else:
            self._connection.set(f'{self._key_prefix}{key}', value, expire)

    def lpush(self, key: str, items: list):
        return self._connection.lpush(f'{self._key_prefix}{key}', *items)

    def rpush(self, key: str, items: list):
        return self._connection.rpush(f'{self._key_prefix}{key}', *items)

    def blpop(self, key: str):
        return self._connection.blpop(f'{self._key_prefix}{key}', timeout=1)

    def brpop(self, key: str):
        return self._connection.brpop(f'{self._key_prefix}{key}', timeout=1)

    def expire(self, key: str, expire: int):
        self._connection.expire(f'{self._key_prefix}{key}', expire)

if __name__ == '__main__':
    redis_db = Redis()
    redis_db.open()
    redis_db.set('test', 'hallo world', 30)
    print(redis_db.get('test'))
    redis_db.close()