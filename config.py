from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = 'biopro-test-api'
    server_host: str = '0.0.0.0'
    server_port: int = 3030

    db_uri: str = 'postgresql://biopro_test:biopro_test@localhost:5432/main'

    class Config:
        env_file = '.env'


@lru_cache
def get_settings() -> Settings:
    return Settings()
