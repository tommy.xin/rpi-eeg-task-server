from typing import Optional

from sqlalchemy import MetaData, delete, func, update
from sqlalchemy.future import Engine, create_engine, select
from sqlalchemy.orm import Session, sessionmaker

from config import get_settings
from models import Base, Task

settings = get_settings()


class TaskOperator():
    def __init__(self, session: sessionmaker):
        self._session: sessionmaker = session

    def create(self, data: Task) -> Task:
        with self._session() as session:
            session: Session
            session.add(data)
            session.commit()
            return data

    def update(self, id: int, data: dict):
        with self._session() as session:
            session: Session
            data.update({
                'update_date': func.now(),
            })
            session.execute(update(Task).where(Task.id == id).values(data))
            session.commit()

    def delete(self, id: int):
        with self._session() as session:
            session: Session
            session.execute(delete(Task).where(Task.id == id))
            session.commit()

    def get(self, id: int) -> Optional[Task]:
        with self._session() as session:
            session: Session
            return session.get(Task, id)

    def get_all(self) -> list[Task]:
        with self._session() as session:
            session: Session
            row = session.execute(select(Task).order_by(Task.create_date.desc()))
            ret = row.scalars().all()
            session.commit()
            return ret


class Database():
    """Database manager based on sqlalchemy.orm, connect to DB and send sql command.
    """

    def __init__(self):
        # sqlalchemy object
        self._engine: Engine = None
        self._meta: MetaData = Base.metadata
        self._session: sessionmaker = None

        self.task_operator: TaskOperator = None

    def open(self):
        """Setup database and start it.
        """
        self._engine = create_engine(settings.db_uri, echo=False)
        try:
            # generate sync engine session
            self._session = sessionmaker(
                self._engine,
                expire_on_commit=False,
                class_=Session
            )
            print(f'connect success: {settings.db_uri}')
        # connection fail
        except Exception as e:
            print(f'connect fail: {str(e)}', 'error')
            self._engine = None

        self.check_table_sync()

        self.create_operator(self._session)

    def close(self):
        """Database shutdown and close database engine.
        """
        self.dispose()

    def create_operator(self, session: sessionmaker):
        """Create table operator.
        """
        self.task_operator = TaskOperator(session)

    def check_table_sync(self):
        """Check all table with our model and keep structure sync.
        """
        self._meta.bind = self._engine
        self._meta.create_all()

    def dispose(self):
        """Close database engine.
        """
        self._engine.dispose()
        print(f'connect closed')


if __name__ == '__main__':
    db = Database()
    db.open()
    db.task_operator.create(Task(
        name='task_1',
        desc='test task',
        data='1 2 3 4 5 6 7',
        date='2020-03-18',
        checked=False
    ))
    db.close()
