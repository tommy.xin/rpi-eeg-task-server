from pydantic import BaseModel


class TaskContentBase(BaseModel):
    id: int
    name: str
    desc: str
    date: str
    data: list[str]
    checked: bool = False


class TaskContent(TaskContentBase):
    pass


class TaskContentResponse(TaskContentBase):
    data: str

    class Config:
        orm_mode = True
