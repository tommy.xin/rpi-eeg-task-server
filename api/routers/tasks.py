from fastapi import APIRouter, HTTPException, status, Depends
from models import Task

from schemas import TaskContent, TaskContentResponse
from .deps import get_db

router = APIRouter()


@router.get('/task/all')
def all_task_list(db=Depends(get_db)):
    tasks = db.task_operator.get_all()[:100]
    return {'data': [TaskContentResponse.from_orm(task) for task in tasks]}


@router.post('/task/add')
def add_task(task_content: TaskContent, db=Depends(get_db)):
    db.task_operator.create(Task(name=task_content.name,
                                 data=' '.join(task_content.data),
                                 desc=task_content.desc,
                                 date=task_content.date,
                                 checked=task_content.checked))


@router.patch('/task/update/{id}')
def update_task(id: int, task_content: TaskContent, db=Depends(get_db)):
    task = db.task_operator.get(id)
    if task is None:
        raise HTTPException(status_code=404, detail='Task not found')

    res = task_content.dict()
    res['data'] = ' '.join(res['data'])
    db.task_operator.update(id, res)


@router.delete('/task/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_task(id: int, db=Depends(get_db)):
    task = db.task_operator.get(id)
    if task is None:
        raise HTTPException(status_code=404, detail='Task not found')

    db.task_operator.delete(id)
