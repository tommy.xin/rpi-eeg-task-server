from db.database import Database


def set_db(database: Database):
    global db
    db = database


def get_db() -> Database:
    return db
