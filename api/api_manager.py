from threading import Thread
from time import sleep

from fastapi import FastAPI
from fastapi.security import HTTPBasic
from starlette.middleware.cors import CORSMiddleware
from uvicorn import Config, Server

from config import get_settings
from db.database import Database

from .routers import tasks
from .routers.deps import set_db

settings = get_settings()


def create_app() -> FastAPI:
    app = FastAPI(title=settings.app_name, debug=False)
    app.include_router(tasks.router, tags=['tasks'])
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )
    return app


app = create_app()
security = HTTPBasic()


class ApiServer(Server):
    def __init__(self, app: FastAPI, host: str, port: int):
        Server.__init__(self, Config(app=app, host=host, port=port, log_level='critical'))
        self.thread: Thread = None

    def start(self):
        self.thread = Thread(target=self.run)
        self.thread.start()

    def close(self):
        self.should_exit = True
        self.thread.join()


class ApiManager():
    def __init__(self):
        self._host = settings.server_host
        self._port = settings.server_port
        self._db = Database()
        self._server: ApiServer = None

    def setup(self):
        self._db.open()
        set_db(self._db)
        self._server = ApiServer(app=app, host=self._host, port=self._port)
        self._server.start()
        print(f'API server running on http://{self._host}:{str(self._port)}')

    def shutdown(self):
        self._db.close()
        self._server.close()
        print(f'API server shutdown complete.')


if __name__ == "__main__":
    api_manager = ApiManager()
    api_manager.setup()
    while True:
        try:
            sleep(10)
        except:
            break
    api_manager.shutdown()
