from collections import deque
from datetime import datetime
from statistics import mean

from db.database import Database, Task


class DataAnalyzer:
    def __init__(self, db: Database, threshold: int = 2000):
        self.db = db
        self.threshold = threshold

        self.counter = 0
        self.need_len = 0
        self.msg_queue: deque[str] = deque(maxlen=501)

    def receive_data(self, msg: list[str]):
        """Receive data from MQTT every 200ms and save to the database.

        Args:
            msg (list[str]): data from an EEG device via MQTT.
        """
        if any(int(v) >= self.threshold for v in msg):
            self.counter = 0
        else:
            self.counter += 1

        # Sleep Detect
        if self.counter == 600:
            self.counter = 0
            self.msg_queue.extend(msg)

            data = list(self.msg_queue)
            avg = mean(int(v) for v in data)
            self.db.task_operator.create(
                Task(name='Sleep Detect',
                     data=' '.join(data),
                     desc=f'The average value of the signal is {avg:.2f}.',
                     date=datetime.now().strftime('%Y-%m-%d')))
            return

        for v in msg:
            self.msg_queue.append(v)
            if self.need_len > 0:
                self.need_len -= 1
                # Activation Detect
                if self.need_len == 0 and len(self.msg_queue) == 501:
                    data = list(self.msg_queue)
                    signal = max(int(v) for v in data)
                    avg = mean(int(v) for v in data)
                    self.db.task_operator.create(
                        Task(name='Activation Detect',
                             data=' '.join(data),
                             desc=f'The signal is {signal}, and the average value before and after the signal is {avg:.2f}.',
                             date=datetime.now().strftime('%Y-%m-%d')))

            elif int(v) >= self.threshold:
                self.need_len = 250
