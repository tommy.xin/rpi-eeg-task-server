from time import sleep
from typing import Any, Dict
from uuid import uuid4

import paho.mqtt.client as mqtt

from .analyzer import DataAnalyzer
from db.database import Database

_RUNTIME_COMPILE = False


class MqttManager():
    """MQTT manager based on paho.mqtt, provide functions to control mqtt message.
    """

    def __init__(self):
        # mqtt config
        self._client_name = f'{str(uuid4())}'
        self._mqtt_client: mqtt.Client = None
        self._is_shutdown = True
        self._db = Database()
        self.data_analyzer = DataAnalyzer(self._db)

    def setup(self):
        """Setup MQTT manager: connect, loop start
        """
        self._db.open()
        self._is_shutdown = False
        self.connect()
        self._mqtt_client.loop_start()

    def shutdown(self):
        """Shutdown MQTT manager: loop stop, disconnect, clear thread pool
        """
        self._db.close()
        self._is_shutdown = True
        self._mqtt_client.loop_stop(force=True)
        self._mqtt_client.disconnect()

    def connect(self):
        """Connect to MQTT broker
        """
        self._mqtt_client = mqtt.Client(self._client_name)
        self._mqtt_client.connect_async('127.0.0.1', 1883, 60)
        self._mqtt_client.on_connect = self.on_connect
        self._mqtt_client.on_disconnect = self.on_disconnect
        self._mqtt_client.on_message = self.on_message

    def on_connect(self, client: mqtt.Client, user_data, flags: Dict[str, Any], rc: int):
        """Run while connect to broker, subscribe all manager topic.

        Args:
            client (mqtt.Client): mqtt client
            user_data (_type_): user data
            flags (Dict[str, Any]): options
            rc (int): rc
        """
        print('connected')
        self._mqtt_client.subscribe(f'eeg/da:rf:3a:9c')

    def on_disconnect(self, client: mqtt.Client, user_data, rc):
        """Run while disconnect to broker and manager not is_shutdown, try to reconnect.

        Args:
            client (mqtt.Client): mqtt client
            user_data (_type_): user data
            rc (_type_): rc
        """
        if not self._is_shutdown:
            sleep(3)
            self._mqtt_client.reconnect()

    def publish(self, topic: str, payload: str, qos=2, show_log=True):
        """Publish data to broker.

        Args:
            topic (str): publish topic
            payload (str): message
            qos (int, optional): at most once (0), at least once (1), exactly once (2). Defaults to 2.
            show_log (bool, optional): is show log. Defaults to True.
        """
        if self._mqtt_client and self._mqtt_client.is_connected():
            self._mqtt_client.publish(topic=topic, payload=payload, qos=qos)

    def on_message(self, client: mqtt.Client, user_data, message: mqtt.MQTTMessage):
        """Receive data from MQTT broker and dispatch by topic.

        Args:
            client (mqtt.Client): mqtt client
            user_data (_type_): user data
            message (mqtt.MQTTMessage): mqtt message payload
        """
        mes: str = str(message.payload.decode())
        self.data_analyzer.receive_data(mes.split(' '))


if __name__ == '__main__':
    mqtt_manager = MqttManager()
    mqtt_manager.setup()
    while True:
        try:
            sleep(10)
        except:
            break
    mqtt_manager.shutdown()
