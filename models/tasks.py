from sqlalchemy import Column
from sqlalchemy import (BigInteger, Boolean, Column, DateTime, func)
from sqlalchemy.dialects import postgresql

from .base import Base


class Task(Base):
    __tablename__ = 'bps_task'

    id = Column(BigInteger, primary_key=True)
    name = Column(postgresql.VARCHAR(64), server_default='', nullable=False)
    desc = Column(postgresql.TEXT, server_default='', nullable=False)
    data = Column(postgresql.TEXT, server_default='', nullable=False)
    checked = Column(Boolean, server_default='false', nullable=False)
    date = Column(postgresql.VARCHAR(64), server_default='', nullable=False)

    create_date = Column(DateTime, server_default=func.now())
    update_date = Column(DateTime, server_default=func.now())
